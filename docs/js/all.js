"use strict";

var quotes = [{
  quote: "If you wish to make an apple pie from scratch, you must first invent the universe.", // contains the text of the quote that will be displayed on the page.
  source: "Carl Sagan", // contains the creator of the quote. For example: "Mark Twain" or "Traditional Irish proverb.”
  citation: "Cosmos (book)", // OPTIONAL: contains a string identifying where the quote comes from, like a speech or publication. For example, "Famous Anonymous Jokes." If there is no known publication, do not include this property on the object.
  year: 1980,
  tags: ['space', 'cosmology', 'science', 'chemistry']
}, {
  quote: "There is no greater education than one that is self-driven.",
  source: "Neil deGrasse Tyson",
  citation: "Interview with Times columnist Jerry Large",
  year: 2011,
  tags: ['education', 'motivation']
}, {
  quote: "We are all now connected by the Internet, like neurons in a giant brain.",
  source: "Stephen Hawking",
  citation: "USA Today interview",
  year: 2014,
  tags: ['internet', 'technology']
}, {
  quote: "The Internet is becoming the town square for the global village of tomorrow.",
  source: "Bill Gates",
  tags: ['internet', 'future', 'computers', 'technology']
}, {
  quote: "I do not fear computers. I fear the lack of them.",
  source: "Isaac Asimov",
  tags: ['computers', 'technology']
}];

document.body.onload = function () {
  var id = setInterval(printquote, 2000);
};

// event listener to respond to "Show another quote" button clicks
document.getElementById('quote-button').addEventListener("click", printQuote, false);

// Simply selects a random quote object from the quotes array
// and returns the randomly selected quote object
function getRandomQuote() {
  var random = Math.floor(Math.random(quotes.length));
  console.log('getRandomQuote():\nreturning: quotes[' + random + ']\n');
  return quotes[random];
}

// This event listener constructs a string of HTML
// containing the different properties of the quote object.
function printQuote() {
  // Call getRandomQuote, then store the returned quote object in a variable
  var theQuote = getRandomQuote();

  // if the quote doesn't exist, print the error and exit
  if (!theQuote) {
    console.error('Error: Failed to get random quote');
    return;
  }

  // Declare & Assign
  var quoteElement = document.getElementById('quote');
  var sourceElement = document.getElementById('source');
  var citationElement = document.getElementById('citation');
  var yearElement = document.getElementById('year');

  quoteElement.innerHTML = theQuote.quote;
  sourceElement.innerHTML = theQuote.source;

  console.log('quoteElement: ' + quoteElement);
}
//# sourceMappingURL=all.js.map
