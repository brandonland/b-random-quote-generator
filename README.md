# b-random-quote-generator
This is my first project for the [Team Treehouse](https://teamtreehouse.com) ([Tech Degree](https://teamtreehouse.com/techdegree)) **Full Stack JavaScript** Curriculum.
## Requirements
- Node.js

## Installation

### Option 1 (recommended):
1. [Install yarn](https://yarnpkg.com/en/docs/install)
2. `cd` into cloned/forked repo, run `yarn`
3. Run `yarn start`
4. Open http://localhost:8080

### Option 2:
1. `cd` into cloned/forked repo, run `npm i`
2. Run `npm start`
3. Open http://localhost:8080
